#include <WiFiNINA.h>
#include <utility/wifi_drv.h>
int state = 0;

int Trigger = 5;             //Pin digital 2 para el Trigger del sensor
int Echo = 4;               //Pin digital 3 para el Echo del sensor

void setup() {
  WiFiDrv::pinMode(25, OUTPUT);
  WiFiDrv::pinMode(26, OUTPUT);
  WiFiDrv::pinMode(27, OUTPUT);
  Serial.begin(9600);        //iniciailzamos la comunicación serial
  pinMode(Trigger, OUTPUT);  //pin Trigger como salida
  pinMode(Echo, INPUT);      //pin Echo como entrada
  digitalWrite(Trigger, LOW);//Inicializamos el pin con 0
}

void loop()
{

  long t;                    //tiempo que demora en llegar el eco
  long d;                    //distancia en centimetros

  digitalWrite(Trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);     //Enviamos un pulso de 10microsegundos
  
  
  t = pulseIn(Echo, HIGH);   //obtenemos el tiempo que tarda en llegar el pulso HIGH en microsegundos
  d = t*0.034/2;             //calculamos la distancia del objeto en base a la constante de la velocidad del sonido en el aire

  if(d>2&&d<10){
    WiFiDrv::analogWrite(25, 255);
    WiFiDrv::analogWrite(26, 0);
    WiFiDrv::analogWrite(27, 0);
    }
  if(d>10&&d<15){
    WiFiDrv::analogWrite(25, 0);
    WiFiDrv::analogWrite(26, 255);
    WiFiDrv::analogWrite(27, 0);
    }
  if(d>15&&d<20){
    WiFiDrv::analogWrite(25, 0);
    WiFiDrv::analogWrite(26, 0);
    WiFiDrv::analogWrite(27, 255);
    }
  if(d>20){
    WiFiDrv::analogWrite(25, 255);
    WiFiDrv::analogWrite(26, 255);
    WiFiDrv::analogWrite(27, 255);
    }
  
  Serial.print("Distancia: ");
  Serial.print(d);           //Enviamos serialmente el valor de la distancia
  Serial.print("cm");
  Serial.println();
  delay(50);                //Hacemos una pausa de 100ms
}
