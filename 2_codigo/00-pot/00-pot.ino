//Ejemplo para lectura potenciómetro analógico y lectura por el monitor serie

int pot = 5; //Pin AnalogIn A5 potenciómetro 
int lecturaPot;

void setup() {
  Serial.begin(9600); //Iniciamos comunicación serial a 9600 baudios
}

void loop() {
  lecturaPot = analogRead(pot);
  Serial.print("Valor pot: ");
  Serial.println(lecturaPot);
}
