//Ejemplo para controlar brillo del LED con un potenciómetro
int led = 3; // Pin digital ~3
int brillo; // Brillo del led rojo
int pot = 0; //Pin AnalogIn A0 potenciómetro 

void setup() {
  Serial.begin(9600); //Iniciamos comunicación serial a 9600 baudios
  pinMode (led, OUTPUT); //Decimos a arduino que el pin 3 es de salida
}

void loop() {
  brillo = analogRead(pot)/4; //Dividimos entre 4 para la conversión de resolución analógica (0-1023) a digital (0-255)
  //brillo = map(analogRead(pot),0,1023,0,255); //Otra manera de conversión de resolución
  analogWrite(led,brillo);
  Serial.print("Brillo: ");
  Serial.println(brillo);
}
