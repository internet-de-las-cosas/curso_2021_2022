//Ejemplo de salida de señal digital por pines digitales PWM~
//El efecto es similar a la luz led de estado de reposo de los macbooks 2012s

int led = 3; // Pin digital ~3
int brillo; // Brillo del led

void setup() {
  Serial.begin(9600); //Iniciamos comunicación serial a 9600 baudios
  pinMode (led, OUTPUT); //Decimos a arduino que el pin 3 es de salida
}

void loop() {
    for(brillo=0; brillo<256;brillo++){ // bucle para itinerar: for(init;check;update){ejecutar funciones}
      analogWrite(led,brillo);
      delay(15);
      }
    delay(100);
    for(brillo=255; brillo>0;brillo--){
      analogWrite(led,brillo);
      delay(15);
      }
}
