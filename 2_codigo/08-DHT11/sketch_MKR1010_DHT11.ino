#include <DHT.h>
#include <DHT_U.h>

int dhtpin = 5;
float temp;
float hum;

DHT dht(dhtpin, DHT11);

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  temp = dht.readTemperature();
  hum = dht.readHumidity();
  Serial.print("Temperatura: ");
  Serial.print(temp);
  Serial.println("C");
  Serial.print("Humedad:     ");
  Serial.print(hum);
  Serial.println("%");
  delay(500);

}
